package tech.mastertech.kafka;

import java.time.Duration;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;

public class ProducerMain {

	private static final int P_THREADS = 2;
	
	private static final String TOPIC = "topico-teste";
	//private static final String SERVERS = "localhost:9090;localhost:9091;localhost:9092";
	private static final String SERVERS = "localhost:9092";

	/**
	 * 
	 * @param index
	 * @return
	 */
	public static KafkaProducer<Long, String> createProducer(int index) {
		Properties props = new Properties();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, SERVERS);
		props.put(ProducerConfig.CLIENT_ID_CONFIG, "Produtor-" + index);
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, LongSerializer.class);
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);

		KafkaProducer<Long, String> producer = new KafkaProducer<>(props);
		return producer;
	}

	public static Thread createProducerThread(final int index) {
		Thread t = new Thread(new Runnable() {

			@Override
			public void run() {
				System.out.printf("Thread [%s] starting.\n", Thread.currentThread().getName());
				Random random = new Random();
				KafkaProducer<Long, String> producer = createProducer(index);
				for (long i = 0; i < 10; i++) {
					
					final String topico = TOPIC;
					final long key = random.nextInt(10000000);
					final String body = "Corpo da mensagem " + key;
					
					ProducerRecord<Long, String> record = new ProducerRecord<>(topico, key, body);
					
					try {
						producer.send(record).get();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (ExecutionException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
//					producer.send(record, (m, e) -> {
//						System.out.println(" [x] Sent " + index + "/" + mid);
//					});
				}
				producer.flush();
				producer.close();
			}
		});
		t.setName("ProducerThread-" + index);
		return t;
	}

	public static void main(String[] args) {
		long time = System.currentTimeMillis();
		
		List<Thread> threads = new LinkedList<>();	
		
		System.out.println("Starting threads...");
		
		// Criar thread de produtores
		for (int i = 0; i < P_THREADS; i++) {
			Thread t = createProducerThread(i);
			threads.add(t);
		}
		
		// Inicia todas as threads
		for(Thread t : threads) {
			t.start();
		}

		try {
			System.out.println("Waiting threads...");

			for(Thread t : threads) {
				t.join();
			}
			
			time = System.currentTimeMillis() - time;

			System.out.println("Finished: " + time + "ms");

		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
